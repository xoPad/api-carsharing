const mongoose = require('mongoose');
const bCrypt = require('bcrypt');
const MongoDb = require('mongodb');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const ObjectId = require('mongodb').ObjectID;
const authHelper = require('../helpers/authHelper');

const { secret } = require('../../config/app').jwt;

const Car = mongoose.model('Car');
const Book = mongoose.model('Book');
const Favorites = mongoose.model('Favorites');

/**
 *Получение информации об автомобилях
 *
 * @param {*} req
 * @param {*} res
 * @returns
 */
const cars = (req, res) => {
    Car.find()
        .exec()
        .then(cars=>res.json(cars))
        .catch(err => res.status(500).json(err));
};


/**
 *Получение информации об истории аренд
 *
 * @param {*} req
 * @param {*} res
 * @returns
 */
const books = (req, res) => {
    const authHeader = req.get('Authorization');
    const token = authHeader.replace('', '');
    if (!token) return res.status(401).send('Access denied!');
    try {
        decode = jwt.verify(token, secret);
    } catch (e) {
        return res.status(401).send('unauthorized');
    }

    var userId = decode.userId;
    Book.find({ user_id: userId })
    .exec()
    .then(book=>res.json(book)
    .catch(err => res.status(500).json(err)));
};

/**
 * 
 * @param {*} req Запрос
 * @param {*} res Ответ
 */
const removeallbook = (req, res) => {
    const authHeader = req.get('Authorization');
    const token = authHeader.replace('', '');
    if (!token) return res.status(401).send('Access denied!');
    try {
        decode = jwt.verify(token, secret);
    } catch (e) {
        return res.status(401).send('unauthorized');
    }

    var userId = decode.userId;

    Book.deleteMany({user_id: userId}, req.body)
        .exec()
        .then(() => res.json({success: true}))
        .catch(err => res.status(500).json(err));
};

/**
 * 
 * @param {*} req Запрос
 * @param {*} res Ответ
 */
const removebook = (req, res) => {
    Book.deleteOne({_id:req.body._id}, req.body)
    .exec()
    .then(() => res.json({success: true}))
    .catch(err => res.status(500).json(err));
};

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
const newbook = async (req, res) => {

    const authHeader = req.get('Authorization');
    const token = authHeader.replace('', '');
    if (!token) return res.status(401).send('Access denied!');
    try {
        decode = jwt.verify(token, secret);
    } catch (e) {
        return res.status(401).send('unauthorized');
    }

    var userId = decode.userId;

    const book = new Book({
        user_id: userId,
        car: req.body.car,
        time_wait: req.body.time_wait,
        time_drive: req.body.time_drive,
        wait_price: req.body.wait_price,
        drive_price: req.body.drive_price
    });
    try {
        const savedBook = await book.save();
        res.send({book: book._id});
    } catch (e) {
        res.status(400).send(e);
    }

};


/**
 *Получение информации о любимых машинах
 *
 * @param {*} req
 * @param {*} res
 * @returns
 */
const favorites = (req, res) => {
    const authHeader = req.get('Authorization');
    const token = authHeader.replace('', '');
    if (!token) return res.status(401).send('Access denied!');
    try {
        decode = jwt.verify(token, secret);
    } catch (e) {
        return res.status(401).send('unauthorized');
    }

    var userId = decode.userId;
    Favorites.find({ user_id: userId })
    .exec()
    .then(fav=>res.json(fav)
    .catch(err => res.status(500).json(err)));
};

/**
 * 
 * @param {*} req Запрос
 * @param {*} res Ответ
 */
const removefavorites = (req, res) => {
    Favorites.deleteOne({_id:req.body._id}, req.body)
    .exec()
    .then(() => res.json({success: true}))
    .catch(err => res.status(500).json(err));
};

/**
 * 
 * @param {*} req Запрос
 * @param {*} res Ответ
 */
const carinfo = (req, res) => {
    Car.findById({_id:req.body._id})
    .exec()
    .then(fav=>res.json(fav)
    .catch(err => res.status(500).json(err)));
};

/**
 * 
 * @param {*} req Запрос
 * @param {*} res Ответ
 */
const removeallfavorites = (req, res) => {
    const authHeader = req.get('Authorization');
    const token = authHeader.replace('', '');
    if (!token) return res.status(401).send('Access denied!');
    try {
        decode = jwt.verify(token, secret);
    } catch (e) {
        return res.status(401).send('unauthorized');
    }

    var userId = decode.userId;

    Favorites.deleteMany({user_id: userId}, req.body)
        .exec()
        .then(() => res.json({success: true}))
        .catch(err => res.status(500).json(err));
};

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
const newfavorites = async (req, res) => {

    const authHeader = req.get('Authorization');
    const token = authHeader.replace('', '');
    if (!token) return res.status(401).send('Access denied!');
    try {
        decode = jwt.verify(token, secret);
    } catch (e) {
        return res.status(401).send('unauthorized');
    }

    var userId = decode.userId;

    const fav = new Favorites({
        user_id: userId,
        car_id: req.body.car_id,
        car_name: req.body.car_name,
        name: req.body.name
    });
    try {
        const savedFav = await fav.save();
        res.send({fav: fav._id});
    } catch (e) {
        res.status(400).send(e);
    }

};

module.exports = {
    cars,
    books,
    carinfo,
    newbook,
    favorites,
    removebook,
    newfavorites,
    removeallbook,
    removefavorites,
    removeallfavorites,
};