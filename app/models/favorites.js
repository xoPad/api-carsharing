const mongoose = require('mongoose');

const FavoritesSchema = new mongoose.Schema({
    user_id:{
        type: String,
        required: true
    },
    car_id:{
        type: String,
        required: true
    },
    car_name:{
        type: String,
        required: true
    },
    name:{
        type: String,
        required: true
    }
});

mongoose.model('Favorites',FavoritesSchema);